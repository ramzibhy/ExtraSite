module.exports = {
    "extends": "airbnb-base",
    "parser": "babel-eslint",
    "rules": {
      'linebreak-style': 0,
      "no-underscore-dangle": [2, { "allow": ["_id"] }],
    }
};