// require('dotenv').config({path: '../../.env'})
const mongoose = require('mongoose');

const mlap = (info) => {
  const mongoDB = `mongodb://${info.username}:${info.password}@${info.host}/${info.database}`;
  return mongoDB;
};

/*
const connect = () => {
  mongoose.connect(mlap({
    host: process.env.DB_HOST,
    username: process.env.DB_USER,
    password: process.env.DB_PASS,
    database: process.env.DB_NAME,
  }));
  mongoose.Promise = global.Promise;
  const db = mongoose.connection;
  db.once('open', () => {
    // console.log('connected');
  });
};
*/

const connect = () => {
	mongoose.connect("mongodb://localhost:27017/fantasia");
	mongoose.Promise = global.Promise;
	const db = mongoose.connection;
	db.once('open', () => {
		// console.log('connected');
	})
};

// var db = mongoose.connection;
// db.on('error', console.error.bind(console, 'Connection Error'));

module.exports = {
  connect() { connect(); },
};
