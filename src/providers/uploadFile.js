import multer from 'multer';
// import path from 'path';

const env = require('dotenv').config().parsed; // To verify the require and process.env not readed problem

const storage = multer.diskStorage({
  // destination: '/home/ramzibhy/Documents/accessoriesSite/back/static/products',
  // destination: 'C:/Users/tzfdsfs/Documents/accessoriesSite/back/static/products',
  destination: `${env.projectFolder}/${env.productImagesFolder}`,
  filename(req, file, next) {
    next(null, `${file.fieldname}_${Date.now()}.jpg`);
  },
});
/*
let validateFile = function(file, cb){
  allowedFileTypes = /jpeg|jpg|png|gif/;
  const extension = allowedFileTypes.test(path.extname(file.originalname).toLowerCase());
  const mimeType = allowedFileType.test(file.mimetype);
  if(extension && mimeType){
    return cb(null, true);
  }
  else{
    return cb('Invalid file type. Only JPEG, PNG and GIF file are allowed.');
  }
};

const upload = multer({
  storage,
  limits: { fileSize: 2000000 },
  fileFilter(req, file, callback){
    validateFile(file, callback);
  },
}).single('picture');
*/

const upload = multer({ storage }).single('picture');
module.exports = upload;
