import jwt from 'jsonwebtoken';
import bcrypt from 'bcrypt';
import User from '../../models/user';

export default {
  Query: {
    getAllUsers: async (parent, args, { user }) => {
	if (!user) {
		throw new Error('You are not authenticated!');
	}
      const users = await User.find();
      return users;
    },
    getCurrentUser: async (parent, args, { user }) => {
      if (!user) {
        throw new Error('You are not authenticated!');
      }
      const currentUser = User.findOne({ username: user.username });
      return currentUser;
    },
  },
  Mutation: {
    login: async (parent, args) => {
      const user = await User.findOne({ username: args.username });
      if (!user) {
        throw new Error('No user with that username is in our database');
      }
      const valid = await bcrypt.compare(args.password, user.password);
      if (!valid) {
        throw new Error('Incorrect password');
      }
      return jwt.sign(
        {
          username: user.username,
          firstName: user.firstName,
          lastName: user.lastName,
          inscriptionDate: user.inscriptionDate,
        },
        process.env.secretKey,
        { expiresIn: '1d' },
      );
    },
/*
    signup: async (parent, args) => {
      const user = await new User(args);
      user.inscriptionDate = Date.now();
      if (!user.privilege) {
        user.privilege = 'user';
      }
      await user.save(user);
      return jwt.sign(
        {
          username: user.username,
          firstName: user.firstName,
          lastName: user.lastName,
          inscriptionDate: user.inscriptionDate,
        },
        process.env.secretKey,
        { expiresIn: '1d' },
      );
    },
*/
  },
};
