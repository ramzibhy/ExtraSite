import * as fs from 'fs';
import Product from '../../models/product';

require('dotenv').config({ path: '../../../.env' });

export default {
  Query: {
    getAllProducts: async () => {
      /*
      if (!user) {
        throw new Error('You are not authenticated!');
      }
      */
      const productList = await Product.find();
      return productList;
    },
    getProductByID: async () => {
      /*
      if (!user) {
        throw new Error('You are not authenticated!');
      }
      const product = await Product.findOne(args);
      return product;
      */
    },
  },
  Mutation: {
    addProduct: async (parent, args, { user }) => {
      if (!user) {
        throw new Error('You are not authenticated!');
      }
      await new Product(args).save();
      return 'Product Added';
    },
    editProduct: async (parent, args, { user }) => {
      if (!user) {
        throw new Error('You are not authenticated!');
      }
      const query = { _id: args._id };
      let state = 'Modification Failed';
      const newValues = args;
      delete newValues._id;
      await Product.update(query, newValues, async () => {
        state = await new Promise((resolve) => {
          resolve('Product Modified');
        });
      });
      return state;
    },
    deleteProduct: async (parent, args, { user }) => {
      if (!user) {
        throw new Error('You are not authenticated!');
      }
      const product = await Product.findOne(args);
      const path = `${process.env.projectFolder}/${product.picture}`;
      Product.find(args).remove().exec();
      fs.unlink(path, (err) => {
        // if (err) throw err;
        if (err) {
          // TO DO
        }
        // console.log('Picture was deleted');
      });
      return 'Product Deleted';
    },
  },
};
