import product from './product';
import user from './user';

const resolvers = [product, user];
const queries = {};
const mutations = {};

resolvers.forEach((r) => {
  Object.keys(r.Query).forEach((key) => {
    queries[key] = r.Query[key];
  });
  Object.keys(r.Mutation).forEach((key) => {
    mutations[key] = r.Mutation[key];
  });
});

export default {
  Query: queries,
  Mutation: mutations,
};
