export default {
  type: `
    type User {
      _id: ID!
      username: String!
      firstName: String!
      lastName: String!
      email: String!
      password: String!
      inscriptionDate: String!
      privilege: String!
    }
  `,
  queries: `
    getAllUsers: [User]
    getCurrentUser: User
  `,
  mutations: `
    login(username: String!, password: String!) : String
  `,
};
