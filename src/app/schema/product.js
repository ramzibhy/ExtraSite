export default {
  type: `
    type Product {
      _id: ID!
      name: String!
      description: String!
      picture: String!
      price: Float!
    }
  `,
  queries: `
    getAllProducts: [Product]
    getProductByID(_id: ID!): Product
  `,
  mutations: `
    addProduct(name: String!, description: String!, picture: String!, price: Int!): String
    editProduct(_id: ID!, name: String, description: String, picture: String, price: Int): String
    deleteProduct(_id: ID!): String
  `,
};
