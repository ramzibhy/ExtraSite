import product from './product';
import user from './user';

const schemas = [product, user];
const types = [];
const queries = [];
const mutations = [];

schemas.forEach((s) => {
  types.push(s.type);
  queries.push(s.queries);
  mutations.push(s.mutations);
});

export default `
${types.join('\n')}

type Query {
  ${queries.join('\n')}
}

type Mutation {
  ${mutations.join('\n')}
}
`;
