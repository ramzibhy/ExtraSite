import jwt from 'express-jwt';
import express from 'express';
import cors from 'cors';
import bodyParser from 'body-parser';
import { graphqlExpress, graphiqlExpress } from 'apollo-server-express';
import { makeExecutableSchema } from 'graphql-tools';

import typeDefs from './app/schema';
import resolvers from './app/resolvers';
import mongoDB from './providers/mongoDB';
import uploadFile from './providers/uploadFile';
import Product from './models/product';

require('dotenv').config({ path: './.env' });

const PORT = 5000;
const auth = jwt({
  secret: process.env.secretKey,
  credentialsRequired: false,
});

mongoDB.connect();
const app = express();
const schema = makeExecutableSchema({ typeDefs, resolvers });

app.use(cors());
app.use(bodyParser.urlencoded({
  extended: true,
}));

app.post('/upload', uploadFile, (req, res) => {
  const fullPath = `${process.env.productImagesFolder}/${req.file.filename}`;
  const document = {
    name: req.body.name,
    description: req.body.description,
    picture: fullPath,
    price: req.body.price,
  };
  const product = new Product(document);
  product.save((err) => {
    if (err) {
      throw err;
    }
    res.json({ message: 'File uploaded successfully' });
  });
});

app.use('/graphql', bodyParser.json(), auth, graphqlExpress(req => ({
  schema,
  context: {
    user: req.user,
  },
})));

/* ---- Test Request ------
app.use('/graphql', bodyParser.json(), auth, graphqlExpress(req => {
  console.log(req);
  return {
    schema,
    context: {
      user: req.user,
    },
  }
}));
*/
app.use('/graphiql', graphiqlExpress({ endpointURL: '/graphql' }));
app.listen(PORT);
