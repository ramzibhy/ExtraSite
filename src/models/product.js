import mongoose from 'mongoose';

const ProductSchema = new mongoose.Schema({
  name: {
    type: String,
    // unique: true,
    required: true,
    dropDups: true,
  },
  description: { type: String, required: true },
  picture: { type: String, required: true },
  price: { type: Number, required: true },
});

// ProductSchema.pre('save', function(){});
module.exports = mongoose.model('Product', ProductSchema);
