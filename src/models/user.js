/* eslint-disable */
import bcrypt from 'bcrypt';
import mongoose from 'mongoose';

const UserSchema = new mongoose.Schema({
  username: { type: String, required: true, unique: true },
  firstName: { type: String, required: true },
  lastName: { type: String, required: true },
  password: { type: String, required: true },
  email: { type: String, required: true },
  inscriptionDate: { type: Date },
  privilege: { type: String, enum: ['admin', 'user'] },
});

UserSchema.pre('save', function (next, user) {
  this.password = bcrypt.hashSync(user.password, Number.parseInt(process.env.saltRound, 10));
  next();
});

module.exports = mongoose.model('User', UserSchema);
